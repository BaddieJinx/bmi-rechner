
//BMI berechnen Körpergewicht (in kg) geteilt durch Größe (in m) zum Quadrat.

//public class RechnerBMI {
//public static void main(String[] args) {
//float koepergewicht;
//float groesse;

import java.util.Scanner; // Import the Scanner class

class RechnerBMI {
	public static void main(String[] args) {
		Scanner myObj = new Scanner(System.in); // Create a Scanner object
		System.out.println("Gib bitte dein Geschlecht an (w/m/d)");
		String geschlecht = myObj.nextLine();
		String geschlechtlang = null;
		if (geschlecht.equalsIgnoreCase("w")) {
			geschlechtlang = "weiblich";
		} else if (geschlecht.equalsIgnoreCase("m")) {
			geschlechtlang = "männlich";
		} else {
			geschlechtlang = "divers";
		}
		System.out.println("Gib dein Alter an");
		float alter = myObj.nextFloat();
		System.out.println("Gebe dein Gewicht in kg an");
		float gewicht = myObj.nextFloat();
		System.out.println("Gebe deine Köpergroße in m an");
		float groesse = myObj.nextFloat();

		System.out.println("Du bist " + geschlechtlang + " du wiegst " + gewicht +"kg und du bist " + groesse + "m groß. ");
		float BMI = gewicht / (groesse * groesse);
		
		System.out.println("Dein BMI beträgt " + BMI); // Berechnung

		if (geschlecht.equalsIgnoreCase("w")) {
			if (BMI < 19) {
				System.out.println("Du bist untergewichtig, bitte suche einen Arzt auf!");
			} else if (BMI >= 25)
				System.out.println("Du bist übergewichtig, bitte suche einen Arzt auf!");
		} else if (geschlecht.equalsIgnoreCase("m")) {
			if (BMI < 20) {
				System.out.println("Du bist untergewichtig, bitte suche einen Arzt auf!");
			} else if (BMI >= 26)
				System.out.println("Du bist übergewichtig, bitte suche einen Arzt auf!");

		}
	}
}
